---
title: "Committees"
description: "Active committees part of the Apex Dublin Community"
featured_image: '/images/apexdublin-hero-image-1.jpg'
---

Here are all of the currently approved and active Apex Dublin Homeowners Association committees available for community members to join and participate in.

---

Solar Committee
===============

The Solar Committee was originally established to lead the architectural, financial, and technical research efforts on solar energy systems for the entire Apex Dublin Community. It serves in an advisory role to the Apex HOA Board. The primary responsibility of the Solar Committee is to thoroughly review any new architectural modifications applications for PV solar systems being installed on Apex homes and deliver formal comments and recommendations to the Apex HOA Board on these solar system applications.

The major goals and focus include:

* Strive to research the most compatible modern technological solar energy system components available on the market
* Ensure the entire process of choosing PV solar systems is streamlined for Apex Dublin homeowners
* Ensure any Photovoltaic (PV) solar energy systems installed do not void warranties or damage any aspects of Apex Town Homes
* Ensure Solar Providers selected to install new PV solar systems are properly vetted, licensed, reputable, and well warranted in their workmanship
* Review all homeowner submitted Solar energy Architectural Modification Applications and provide comments and recommendations to the Apex HOA Board
* Ensure that any PV solar system installed on Apex Dublin properties is aesthetically pleasing and architectural sound for the specific building configurations

---

Landscape Committee (Dissolved)
===============================

*The Apex HOA Landscape Committee has been officially dissolved as of 04/06/22*

The Landscape Committee serves in an advisory role to the Apex HOA Board. As members of the committee and eyes on the ground, our primary responsibility is not to establish policies, but rather to provide information and options to enable the Apex HOA Board to make fully informed decisions for the common good of the community and Homeowners Association as it relates directly to landscaping.

The major goals and focus include:

* **Maintenance:** Common area inspection to look for deficiencies in the current landscape and document required corrective actions
* **Improvements:** Review potential landscape improvement; propose change or maintenance issues and make recommendation based on cost and benefit to the Apex HOA
* **Transparency:** provide an organized channel of communication between members, residents and the Apex HOA Board at [apexdublin.info](https://apexdublin.info)
* **Communication:** Be in contact with the approved and contracted HOA landscaping company; review maintenance contract and ensure the community is being serviced accordingly; provide a quarterly contractor performance report to the Apex HOA Board
* **Meetings:** Committee will meet as needed; review homeowners' complaints or requests regarding landscaping of the common area and report to the Apex HOA Board as needed
* **Interviews:** Conduct contractor interviews when selecting a new landscape maintenance provider for the Apex community

---
