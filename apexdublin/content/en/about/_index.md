---
title: "About"
description: ""
featured_image: '/images/apexdublin-hero-image-1.jpg'
---

Nestled in the Tri-Valley of the California Bay Area, Apex Dublin is a small community of 115 multi-family town homes. Integrated nicely into the Eastern Dublin area, it provides wonderful accessibility to the parks of Dublin, California and other resources all within a 1.5 mile radius.
