---
title: "Landscape Committee October 2021 Update"
description: "Apex Community Landscape News"
featured_image: "/images/deciduous-tree-emojis.png"
date: 2021-10-15T14:04:05-07:00
tags: ["committee", "landscape"]
---

The Apex Landscape Committee would like to provide an exciting initial update on the progress of landscape within the community. Here are some recent landscape items and events:

* A new landscape company is now servicing the Apex Community Property: [Allied Landscape](http://www.contactallied.com/).
* Allied Landscape will be servicing the Apex Community weekly every Friday.
* The initial landscape site assessment has been completed with Allied Landscape and the Landscape Committee.
* Initial focus of Allied Landscape will be irrigation repair, tree replacement, general landscape maintenance, application of friendly insecticides/herbicides (Non-glyphosate-based), and more.
* Allied Landscape has successfully installed two new dog waste stations on Zenith Ave! Please leverage these strategically placed stations for your canine waste.
* Allied Landscape is planning to get the Apex Community landscape zones back to a baseline health in approximately six weeks.

The Landscape Committee working is tirelessly to ensure our community has a beautiful and well maintained property full of healthy trees, plants, and other botanical components for the pleasure and enjoyment of all residents.
