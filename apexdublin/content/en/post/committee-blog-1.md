---
title: "New Apex Dublin HOA Committees October 2021"
description: "Announcement of New Committees"
featured_image: "/images/committee_background.jpg"
date: 2021-10-09T20:28:20-07:00
tags: ["committee", "solar", "landscape"]
---

Two new Apex Dublin advisory committees have been recently approved by the Apex HOA Board of Directors and officially formed by Apex homeowners:

1. Apex Solar Committee
2. Apex Landscape Committee

The first newly formed committee is the Solar Committee. A small group of homeowners have come together to collectively take on the effort to help streamline the process of selecting and installing Solar energy systems on Apex town homes.

Residential Solar energy is a major component of home ownership here in California. There is a diverse market of Solar Providers ranging from small local business to large corporate entities. It is essential that compatible, fully licensed providers be selected and exhaustively vetted before installing any type of Photovoltaic Solar energy system on Apex homes. The Solar Committee is leading this effort and will have more updates for the community as they progress during the remainder of 2021 and beyond.

In parallel another small group of Apex Dublin homeowners have banded together to form the new Landscape Committee. This committee has taken the lead to oversee and advise on the general landscaping in the community. There is a plethora of different landscaping configurations all throughout the Apex Dublin community property that must be efficiently and effectively maintained. The Landscape Committee will also have personalized updates for the community soon to come. Stay tuned!

For more information on both of these new Apex Dublin HOA committees check out their overview and goals at the [Committee page](/committees/).
