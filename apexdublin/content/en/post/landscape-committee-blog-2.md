---
title: "Landscape Committee Zone Map"
description: "Apex Community Landscaping Zones"
featured_image: "/images/landscape_mapping_background.jpg"
date: 2021-10-29T21:42:31-07:00
tags: ["committee", "landscape"]
---

The Apex Landscape Committee would like to share a new landscaping zoning map. This map illustrates the different zones of the Apex community that our new contracted landscaping company, [Allied Landscape](http://www.contactallied.com/), will be servicing on a weekly, rotating frequency.

Allied landscaping will work in one of these newly defined landscaping zones every Friday. After servicing all zones across a six week time span, they will start over at Zone 1 and continue to maintain the quality, health, and aesthetics of the Apex Community's different landscapes.

![Apex Dublin Landscaping Zones Map](/images/apexdublin-site-map-landscape_zones.jpg)
