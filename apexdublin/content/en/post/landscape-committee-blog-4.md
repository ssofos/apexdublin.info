---
title: "Landscape Committee April 2022 Update"
description: "Letter of Dissolution"
date: 2022-04-21T09:45:17-07:00
featured_image: "/images/landscape_mapping_background.jpg"
tags: ["committee", "landscape"]
---


This is the formal notice from the Landscape Committee, that effective April 6, 2022, the current Apex Landscape Committee is dissolved. This was a difficult decision as our devotion and desire was always to protect and preserve the beauty of this community's landscape.

We are saddened for this is OUR community. Unfortunately, our original purpose as a committee, *to provide advice to the Apex HOA Board so that they could make fully informed decisions for the common good of the community and Homeowners Association as it relates directly to landscaping*, was made useless by the current Apex HOA management company.

This committee can always be reformed by new association and community members anytime in the future if there is the initiative and interest to make things better.

