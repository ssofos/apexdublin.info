---
title: "Landscape Committee February 2022 Update"
description: "Apex Community Landscape News"
featured_image: "/images/deciduous-tree-emojis.png"
date: 2022-02-17T24:01:00-07:00
tags: ["committee", "landscape"]
---

The Apex Landscape Committee would like to provide a February 2022 update on the progress of landscaping within the community. Here are some recent landscape items and events:

* [Allied Landscape](http://www.contactallied.com/) is continuing to service the community's landscape
* 3 months ago Boardwalk Investment (HOA's community management contractor) secretly met with Taylor Morrison about landscaping hand off without the landscape committee
* Site Walk occurred on 02/11/2022 with Taylor Morrison, Boardwalk Investment, Allied Landscaping
* According to Bill Martin from Taylor Morrison, they did an audit of the landscape in 2020, which included the irrigation
* Also according to Bill Martin from Taylor Morrison, HOA accepted the audit on behalf of the association members
* Taylor Morrison is saying that the HOA is now responsible for all landscaping after the audit date and the negligent landscaping done by Jensen is the Apex HOA's responsibility
* Taylor Morrison is attempting to make the Apex HOA take full responsibility of the landscape between 2020 and 2022
* Bill Martin and Ursula Morales are tasked with finding the landscaping audit report from 2020
* Tract Acceptance with City of Dublin cannot happen without Apex HOA's sign off on the landscaping
* We received a paper copy of the punch list of remaining items related to Apex's tract development acceptance
* Taylor Morrison is targeting 1 month to wrap up tract acceptance
* Ursula was going to talk to Aloft Hotels about the maintenance of the picnic grass area between Apex and Aloft, despite that property not being own by Aloft
* According to Ursula the irrigation and replanting should not be done until guarantees are provided by Taylor Morrison to avoid the HOA from not being able to recoup the additional landscaping costs
* Despite this replanting of some new trees has began within the community
* Playground garbage cans will be emptied by Allied Landscaping even though it is not Apex HOA property. Ownership of the property is still be be determined again by Boardwalk Investment
* Ursula has requested a quote from Allied Landscape for a rapid 3 consecutive day of landscaping cleanup, so that catch up on all landscaping zones
* Quotes are also being requested for general trash pick across the Apex landscape and playground trash can pickup, which is not technical part of the Apex HOA's property.
