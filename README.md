About
=====

This is the codified website for the Apex Dublin Community.

The source code includes auto-generated static web content, static site generator configurations, infrastructure automation configurations, and tools to create and manage [apexdublin.info](https://www.apexdublin.info/)

Components
==========

* Web Hosting Platform: [Cloudflare Workers Sites](https://developers.cloudflare.com/workers/platform/sites)
* Static Site Generator: [Hugo](https://gohugo.io)
* Site Theme: [Ananke Gohugo Theme](https://themes.gohugo.io/themes/gohugo-theme-ananke/)

Domains
=======

* [apexdublin.info](https://www.apexdublin.info/)

Development
===========

To develop and create content for this site please setup this in your local environment:

* [Git](https://git-scm.com/downloads)
* [Golang](https://golang.org)
* [Hugo](https://gohugo.io/getting-started/installing/)
* [Wrangler](https://github.com/cloudflare/wrangler)
* A text editor like [vim](https://github.com/vim/vim) or an equivalent

Access to a terminal emulator in either macOS or Linux.

Additionally, you will have to know how to write MarkDown as all of the site content in Hugo is MarkDown formatted.

To quickly learn MarkDown use [CommonMark](https://commonmark.org).

Run the Site Locally
--------------------

Inside a terminal emulator, change into git clone directory of this repository, then execute:

```
mage run
```

This will start a live development Hugo server with the Apexdublin site fully built at: **http://localhost:1313/**

On macOS you can automatically open a Web Browser to the local Hugo server by executing:

```
mage open
```

Contact
=======

If you have any problems, questions, ideas, suggestions, please contact the [Apex Dublin Community](https://apexdublin.info/contact/)
